import express from 'express';
import cors from 'cors';

const app = express();

const corsOptions = {
  origin: 'http://account.skill-branch.ru',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

function sum(a, b) {
  var a = parseInt(a) || 0;
  var b = parseInt(b) || 0;
  return a + b;
}

app.get('/task2A', function (req, res, next) {
  const total = sum(req.query.a, req.query.b);
  res.send(total.toString());
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});